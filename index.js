// S3CS feedback
// ...........................
// evaluation. cet. ac. in
// Username
// CETcs7xx
// Password 7xx (xx is ur roll no in 2 digits)
// S4CS feedback
// ...........................
// evaluation. cet. ac. in
// Username
// CETcs88xx
// Password 88xx (xx is ur roll no in 2 digits)


const puppeteer = require('puppeteer');
var fs = require('fs');

console.log("username suffix : " + process.argv[2]);
console.log("password suffix : " + process.argv[3]);
console.log("start : " + process.argv[4]);
console.log("end : " + process.argv[5]);

function getItemId(prob) {

    let x = Math.random() * 100
    if (x > prob) {
        return 1
    }

    return 2

}

(async () => {

    const browser = await puppeteer.launch();

    for (let usrCtr = process.argv[4]; usrCtr < process.argv[5]; usrCtr++) {
        console.log("===========================================================");
        let uc = String(usrCtr).padStart(2, '0')
        let usrName = process.argv[2] + uc
        let password = process.argv[3] + uc

        if (!fs.existsSync("op/" + usrName)) {
            fs.mkdirSync("op/" + usrName);
        }

        console.log("username : " + usrName);

        const page = await browser.newPage();

        // page.setDefaultNavigationTimeout(80000); 

        page.on('dialog', async dialog => {
            console.log(dialog.message());
            await dialog.dismiss();
        });


        await page.goto('http://evaluation.cet.ac.in/login.php');


        await page.$eval('input[name="ecode"]', (el, usrName) => el.value = usrName, usrName);
        await page.$eval('input[name="passwd"]', (el, password) => el.value = password, password);
        await page.click('input[type="submit"]');
        // await page.waitForTimeout(10000);
        await page.waitForNavigation();

        await page.screenshot({ path: "op/" + usrName + "/" + 000 + '.png', fullPage: true });

        console.log("login successfull");

        let options = await page.evaluate(() => Array.from(document.querySelectorAll('option'), element => element.value));
        console.log("Available subjets");
        console.log(options);

        while (options.length > 1) {

            await page.$eval('select', (el, ops) => el.value = ops[1], options);
            await page.click('input[type="submit"]');
            // await page.waitForTimeout(6000);
            // await page.waitForNavigation();
            await page.waitForSelector("#evalform")

            console.log("subject selected");


            for (let i = 1; i <= 11; i++) {
                const items = await page.$$('input[name="q' + i + '"]');
                try {
                    if (i === 4 || i === 5) {
                        await page.$eval('input[name="q' + i + '"]', (el) => el.click());
                    } if (i === 9) {
                        await items[getItemId(5) - 1].click()
                    } else {
                        await items[getItemId(15) - 1].click()
                    }
                } catch (e) {
                    console.log("fallback");
                    await page.$eval('input[name="q' + i + '"]', (el) => el.click());
                }
            }

            await page.$eval('textarea[name="q12"]', (el) => el.value = "None");
            await page.$eval('textarea[name="q13"]', (el) => el.value = "None");
            // await page.screenshot({ path: 'example' + options.length + '.png' });
            await page.screenshot({ path: "op/" + usrName + '/resp' + options.length + '.png', fullPage: true });

            await page.click('input[type="submit"]');
            // await page.screenshot({ path: 'op/resp' + options.length + '.png', fullPage: true });
            // await page.waitForTimeout(10000);
            await page.waitForNavigation();

            console.log("subject submitted");

            options = await page.evaluate(() => Array.from(document.querySelectorAll('option'), element => element.value));

        }

    }

    await browser.close();
})();